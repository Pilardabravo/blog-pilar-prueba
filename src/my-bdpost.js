//----------------------------------------------------------------------------------------------------------------
// Recupera la información de un post concreto
//----------------------------------------------------------------------------------------------------------------

  function obtenerPost() {

     var urlGet = "https://api.mlab.com/api/1/databases/blog/collections/post?q={'location.city':'" + "Huesca" + "'}&apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe"
     console.log(urlGet);

     var peticion = new XMLHttpRequest();
     peticion.open("GET", urlGet, false);
     peticion.setRequestHeader("Content-Type", "application/json");
     peticion.send();

     var respuesta = JSON.parse(peticion.responseText);
     console.log(respuesta);

     if (peticion.status=="200")
     {
        sessionStorage.setItem("sessionIdPost", respuesta[0]._id.$oid);
        console.log("respuesta");
        console.log(respuesta[0]._id.$oid);
        alert(respuesta[0]._id.$oid);
       return respuesta;
     }
     else
     {
        alert("get incorrecto");
     }

  }

  //----------------------------------------------------------------------------------------------------------------
  // Alta de un post
  //----------------------------------------------------------------------------------------------------------------

  function incluirPost () {

  //MLAB BBDD - Blog coleccion - post
      const URLMLABPOST = "https://api.mlab.com/api/1/databases/blog/collections/post?apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe";

      var ok = true;
      var altaCity = document.getElementsByName("city")[0].value;
      var altaCountry = document.getElementsByName("country")[0].value;
      var altaTitule = document.getElementsByName("titulo")[0].value;
      var altaPost = document.getElementsByName("post")[0].value;
      var altaUser = sessionStorage.getItem("sessionUser");



  //    if document.getElementsByName("imagen")[0].value = null ||
  //    document.getElementsByName("post")[0].value = ''{
          var altaImagen ="https://cdn.pixabay.com/photo/201dUse7/08/03/20/00/venice-2578238__340.jpg";
  //    } else {
  //      var altaImagen = document.getElementsByName("post")[0]dUse.valu
  //    }
  //    console.log(altaImagen);

      var d = new Date();
      var altaDate =d.toUTCString();
      var altaLocation = '"location":{"city":"'+altaCity+'", "country":"'+altaCountry+'"}';
      var newPost = '{'+altaLocation+',"titulo":"'+altaTitule+'","post":"'+altaPost+'","imagen":"'+altaImagen+'","user":"'+altaUser+'","date":"'+altaDate+'"}';
      console.log(newPost);


  // Alta en BBDD post de la aplicacion
      var peticion = new XMLHttpRequest();
      peticion.open("POST", URLMLABPOST, false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send(newPost);

      var respuesta = JSON.parse(peticion.responseText);
      console.log(respuesta);
      alert("respuesta");
      alert(peticion.responseText);

      if (peticion.status != "200"){
          alert("alta fallida");
          ok = false;
      }
      return ok;
  }


  //----------------------------------------------------------------------------------------------------------------
  // Modificar la información de un post
  //----------------------------------------------------------------------------------------------------------------

  function modificarPost () {

  //MLAB BBDD - Blog coleccion - post
      alert("modificarPost");

      var ok = true;

      var updateCity = document.getElementsByName("city")[0].value;
      var updateCountry = document.getElementsByName("country")[0].value;
      var updateTitule = document.getElementsByName("titulo")[0].value;
      var updatePost = document.getElementsByName("post")[0].value;

      var updateUser = sessionStorage.getItem("sessionUser");

      var d = new Date();
      var updateDate =d.toUTCString();

      var updateLocation = '"location":{"city":"'+updateCity+'", "country":"'+updateCountry+'"}';



  //    if document.getElementsByName("imagen")[0].value = null ||
  //    document.getElementsByName("post")[0].value = ''{
          var updateImagen ="https://cdn.pixabay.com/photo/201dUse7/08/03/20/00/venice-2578238__340.jpg";
  //        console.log(updateImagen);
  //    } else {
  //      var updateImagen = document.getElementsByName("post")[0]dUse.valu
  //    }



  // Montamos URL de invocacción al update en BBDD post de la aplicacion

      var idPost = sessionStorage.getItem("sessionIdPost");
      console.log(idPost);

      var urlPut = "https://api.mlab.com/api/1/databases/blog/collections/post/" + idPost + "?apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe"
      console.log(urlPut);

      var updatepost = '{'+updateLocation+',"titulo":"'+updateTitule+'","post":"'+updatePost+'","Imagen":"'+updateImagen+'","User":"'+updateUser+'","Date":"'+updateDate+'"}';
      console.log(updatepost);

      var peticion = new XMLHttpRequest();
      peticion.open("PUT", urlPut, false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send(updatepost);

      var respuesta = JSON.parse(peticion.responseText);
      console.log(respuesta);
      alert("respuesta");

      if (peticion.status != "200"){
          alert("update fallida");
          ok = false;
      }
      return ok;
  }


  //----------------------------------------------------------------------------------------------------------------
  // Borrar un post
  //----------------------------------------------------------------------------------------------------------------

  function borrarPost () {
      alert("borrarPost");
  //MLAB BBDD - Blog coleccion - post

      var ok = true;
      var idPost = sessionStorage.getItem("sessionIdPost");
      var urlDel = "https://api.mlab.com/api/1/databases/blog/collections/post/" + idPost + "?apiKey=OVDPNLVLGGW8NBU1bwr9HN9SSVkf1gMe"
      console.log(urlDel);
      alert(urlDel);
      var peticion = new XMLHttpRequest();
      peticion.open("DELETE", urlDel, false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send();

      var respuesta = JSON.parse(peticion.responseText);
      console.log(respuesta);
      alert("respuesta");
      alert(peticion.responseText);

      if (peticion.status != "200"){
          alert("borrado fallido");
          ok = false;
      }
      return ok;
  }
